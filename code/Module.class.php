<?php

namespace FormTools\Modules\ReportAll;

use FormTools\Forms;
use FormTools\Submissions;
use FormTools\Fields;
use FormTools\Core;
use FormTools\Module as FormToolsModule;


class Module extends FormToolsModule
{
    protected $moduleName = "Report All";
    protected $moduleDesc = "Used to create a report conainting every forms";
    protected $author = "Louis Marchand";
    protected $authorEmail = "louis.marchand@cegepdrummond.ca";
    protected $authorLink = "informatique420.cegepdrummond.ca";
    protected $version = "0.0.1";
    protected $date = "2021-03-08";
    protected $originLanguage = "en_us";

    protected $nav = array(
        "module_name" => array("index.php", false)
    );

    private function getFormFieldsName($a_form)
    {
        $fields = Fields::getFormFields($a_form["form_id"]);
        foreach ($fields as $field) {
            $result[] = $field["field_title"];
        }
        return $result;
    }

    private function getFormFieldsValues($a_form)
    {
        $result = Array();
        $form_id = $a_form["form_id"];
        $count = Submissions::getSubmissionCount($form_id);
        for ( $i = 1; $i <= $count; $i = $i + 1 ) {
            $values = Array();
            $submissions = Submissions::getSubmission($form_id, $i);
            foreach ($submissions as $submission) {
                $values[] = $submission["content"];
            }
            $result[] = $values;
        }
        return $result;
    }

    public function getFormsReports()
    {
        $smarty = Core::$smarty;
        $result = "";
        if (Core::$user->isAdmin()) {
            $account_id = "";
        }
        $forms = Forms::searchForms(array(
            "account_id" => $account_id,
            "is_admin" => true
        ));
        if (empty($forms)) {
            $result = "";
        } else {
            $submission = Array();
            foreach ($forms as $form) {
                $fields = Array();
                $fields["form_name"] = $form["form_name"];
                $fields["fields_name"] = Module::getFormFieldsName($form);
                $fields["fields_values"] = Module::getFormFieldsValues($form);
                $submission[] = $fields;
            }
            $smarty->assign("submissions", $submission);
            $result = $smarty->fetch("../templates/table.tpl");
        }

        return $result;
    }

    public function getFormsArray()
    {
        $smarty = Core::$smarty;
        $L = $this->getLangStrings();
        $result = Array();
        if (Core::$user->isAdmin()) {
            $account_id = "";
        }
        $forms = Forms::searchForms(array(
            "account_id" => $account_id,
        ));
        if (!empty($forms)) {
            $result[] = $L["phrase_form_name"] . "," . 
                $L["phrase_submission_index"] . "," .
                $L["phrase_field_name"] . "," . $L["phrase_value"];
            foreach ($forms as $form) {
                $form_name = $form["form_name"];
                $fields_name = Module::getFormFieldsName($form);
                $fields_values = Module::getFormFieldsValues($form);
                $submission_index = 0;
                foreach ($fields_values as $field_values) {
                    $field_index = 0;
                    foreach ($field_values as $field_value) {
                        $result[] = $form_name . "," . 
                            ($submission_index + 1) . "," .
                            $fields_name[$field_index] . "," . $field_value;
                        $field_index = $field_index + 1;
                    }
                    $submission_index = $submission_index + 1;   
                        
                }
            }
            
        }
        return $result;
    }

}

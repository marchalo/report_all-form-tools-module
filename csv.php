<?php

require_once("../../global/library.php");
$module = FormTools\Modules::initModulePage("client");

header('Content-Type: text/csv');
header('Content-Disposition: attachment; filename="data.csv"');
$data = $module->getFormsArray();
$fp = fopen('php://output', 'wb');
foreach ( $data as $line ) {
    $val = explode(",", $line);
    fputcsv($fp, $val);
}
fclose($fp);

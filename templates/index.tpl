<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>{$L.module_name}</title>
    <style>
        .border {
            border: 1px solid black;
        }
        .red {
            background-color:red;
            color:white;
        }
        .yellow {
            background-color:yellow;
        }
        .green {
            background-color:green;
            color:white;
        }
    </style>
</head>
<body>
    <table cellpadding="0" cellspacing="0" class="margin_bottom_large">
        <tr>
            <td width="45"><a href="../../admin/"><img src="images/back.png" border="0" width="34" height="34" /></a></td>
            <td class="title">
                <a href="../../admin/">{$L.phrase_return}</a>
            </td>
        </tr>
        <tr>
            <td width="45"><a href="../../admin/"><img src="images/download.png" border="0" width="34" height="34" /></a></td>
            <td class="title">
                <a href="csv.php">{$L.phrase_download_csv}</a>
            </td>
        </tr>
    </table>

   {$forms_reports}
</body>
</html>

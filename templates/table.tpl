{foreach from=$submissions item=$submission}
    </br>
    </br>
    <h1>{$submission["form_name"]}</h1>
    <table class="border">
        <tr class="border">
            {foreach from=$submission["fields_name"] item=$field_name}
                <th class="border">{$field_name}</th>
            {/foreach}
        </tr>
        {foreach from=$submission["fields_values"] item=$field_values}
            <tr class="border">
                {foreach from=$field_values item=$field_value}
                    {if $field_value eq 'bien'}
                        <td class="border green">{$field_value}</td>
                    {elseif $field_value eq 'moyen'}
                        <td class="border yellow">{$field_value}</td>
                    {elseif $field_value eq 'mal'}
                        <td class="border red">{$field_value}</td>
                    {else}
                        <td class="border">{$field_value}</td>
                    {/if}
                {/foreach}
            </tr>
        {/foreach}
    </table>
{/foreach}

<?php

/*
Form Tools - Module Language File
---------------------------------

File created: Oct 24th, 2:46 AM

If you would like to help translate this module, please visit:
http://translations.formtools.org/
*/

$L = array();

// required fields
$L["module_name"] = "Report All";
$L["module_description"] = "Used to create a report conainting every forms.";

// custom fields
$L["phrase_return"] = "Return to forms";
$L["phrase_download_csv"] = "Download CSV file";
$L["phrase_form_name"] = "Form name";
$L["phrase_submission_index"] = "Submission index";
$L["phrase_field_name"] = "Field name";
$L["phrase_value"] = "Value";

<?php

/*
Form Tools - Module Language File
---------------------------------

File created: Oct 24th, 2:46 AM

If you would like to help translate this module, please visit:
http://translations.formtools.org/
*/

$L = array();

// required fields
$L["module_name"] = "Report All";
$L["module_description"] = "Permet d'obtenir un rapport de tous les formulaires du système";

// custom fields
$L["phrase_return"] = "Retourner aux formulaires";
$L["phrase_download_csv"] = "Télécharger le fichier CSV";
$L["phrase_form_name"] = "Nom du formulaire";
$L["phrase_submission_index"] = "Index de la soumission";
$L["phrase_field_name"] = "Nom du champ";
$L["phrase_value"] = "Valeur";

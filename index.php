<?php

require_once("../../global/library.php");
$module = FormTools\Modules::initModulePage("client");

$page_vars = array(
    "forms_reports" => $module->getFormsReports()
);

$module->displayPage("templates/index.tpl", $page_vars);
